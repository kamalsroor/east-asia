<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('title');
			$table->longText('description');
			$table->string('price')->nullable();
			$table->integer('sub_category_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('products');
	}
}