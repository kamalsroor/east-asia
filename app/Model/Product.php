<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model 
{

    protected $table = 'products';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('title', 'description', 'price', 'sub_category_id');

    public function Category()
    {
        return $this->belongsTo('App\Model\SubCategory', 'sub_category_id');
    }

    public function Images()
    {
        return $this->hasMany('App\Model\Image', 'product_id');
    }

}