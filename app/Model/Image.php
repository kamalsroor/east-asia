<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model 
{

    protected $table = 'images';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('url', 'product_id');

    public function Product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }

}