<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use stdClass;

class Settings extends Model 
{

    protected $table = 'settings';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('Addresses', 'image1', 'image2', 'image3', 'email');

    public function Slider()
    {
        $Slider = new stdClass;
        $Slider->images = [
            0 => $this->image1, 
            1 => $this->image2,
            2 => $this->image3
        ];
        return $Slider;
    }
}