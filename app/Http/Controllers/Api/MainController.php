<?php

namespace App\Http\Controllers\Api;

// use App\Models\BloodType;
// use App\Models\Category;
// use App\Models\City;
// use App\Models\Contact;
// use App\Models\DonationRequest;
// use App\Models\Governorate;
// use App\Models\Notification;
// use App\Models\Post;
// use App\Models\Client;
// use App\Models\RequestLog;
// use App\Models\Log;
// use App\Models\Token;
use App\Model\Category;
use App\Model\Product;
use App\Model\Settings;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class MainController extends Controller
{
    public function Category(Request $request)
    {
        $Category = Category::with('Children')->where(function($Category) use($request){
            if ($request->input('id'))
            {
                $Category->where('id',$request->id);
            }
        })->latest()->get();
        return responseJson(1, 'success', $Category);
    }
    
    public function Products(Request $request)
    {
        $Product = Product::with('Images')->where(function($Product) use($request){
            if ($request->input('sub_category_id'))
            {
                $Product->where('sub_category_id',$request->sub_category_id);
            }
        })->latest()->get();
        return responseJson(1, 'success', $Product);
    }

    public function SliderImages(Request $request)
    {
        $Settings = Settings::first()->Slider();
        return responseJson(1, 'success', $Settings);
    }

}
